# Assessment Project Comments And Assumptions

Please write down any comments or assumptions you would like us to know when reviewing the test in the list below.

## Comments And Assumptions

* To build the project simply run mvn clean install and deploy ROOT.war
* Requires a local mysql database with user `root` and no password (refer to `datasource.xml`)
* Source the following script

    CREATE DATABASE `TIPICO`;

    CREATE TABLE `TIPICO`.`notes` (
      `id` INT NOT NULL AUTO_INCREMENT,
      `data` VARCHAR(100) NOT NULL,
      `dateCreated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`));

* The POC was simple enough to not require a build system such as [Grunt](http://gruntjs.com/).
* A router was not required as it is only one page.
* Spring data rest was used since it provides the REST APIs including paging and sorting out of the box.
* You can see a regular controller in commit `035e2116c14ac8525c28ecda75e6a955f7ad2a7d`
* Possible enhancements include js minification and obfuscation, html minification