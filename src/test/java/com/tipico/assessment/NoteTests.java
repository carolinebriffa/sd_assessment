package com.tipico.assessment;

import com.tipico.assessment.model.Note;
import com.tipico.assessment.model.NoteRepository;
import org.apache.commons.collections.IteratorUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.TransactionSystemException;

import java.util.List;
import java.util.stream.IntStream;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:datasource-test.xml" })
public class NoteTests {

        @Autowired
        private NoteRepository noteRepository;

        @Test
        @SuppressWarnings("unchecked")
        public void testSaveAndLoad_successful(){

                Note note = new Note();
                String data = "data";
                note.setData(data);

                noteRepository.save(note);

                List<Note> notes = IteratorUtils.toList(noteRepository.findAll().iterator());
                Assert.assertTrue(notes.size() == 1);
                Assert.assertEquals(data, notes.get(0).getData());
        }

        @Test(expected=TransactionSystemException.class)
        public void testSave_invalidLength(){

                Note note = new Note();
                String data = IntStream.range(0, 200).mapToObj(x -> x + "").reduce("", (x, y) -> x + y);
                note.setData(data);

                noteRepository.save(note);
        }

}
