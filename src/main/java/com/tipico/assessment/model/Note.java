package com.tipico.assessment.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "notes")
@Data
public class Note {
    @Id
    private int id;
    @Length(max = 100)
    private String data;
    private Date dateCreated = new Date();
}
