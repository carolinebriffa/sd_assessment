'use strict';
var MainCtrl = function ($http) {
    function MainCtrl() {
        var model = {
            notes: [],
            currentNote: ''
        };
        var paging = {
            pages: [],
            pageSize: 10,
            currentPage: 0,
            desc: true
        };

        function getAll(pageNo) {
            paging.currentPage = pageNo;
            console.log("Fetching [Page: ", pageNo, "]");
            var url = 'note?sort=dateCreated,asc&page=' + pageNo + '&size=' + paging.pageSize;
            if (paging.desc) {
                url = 'note?sort=dateCreated,desc&page=' + pageNo + '&size=' + paging.pageSize;
            }

            $http.get(url).then(function (response) {
                var pageCount;
                var notesServer = response.data['_embedded']['note'];
                var lastPageServer = response.data['_links']['last'];
                !!lastPageServer ? pageCount = /page=(\d)?/.exec(lastPageServer.href)[1] : 1;
                model.notes.length = 0;
                notesServer.forEach(function (note) {
                    model.notes.push(
                        {data: note.data, dateCreated: note.dateCreated}
                    )
                });
                paging.pages.length = 0;
                for (var i = 0; i < pageCount; i++) paging.pages.push(i);

            });
        }

        function add() {
            $http.post('note', {data: model.currentNote}).then(function () {
                getAll(paging.currentPage);
            });
            model.currentNote = '';
        }

        function sort() {
            paging.desc = !paging.desc;
            getAll(paging.currentPage);
        }

        getAll(0);

        return {
            paging: paging,
            model: model,
            getAll: getAll,
            add: add,
            sort: sort
        };
    }

    return new MainCtrl();
};

var app = angular.module('Notes', []);
app.controller('MainCtrl', ['$http', MainCtrl]);